import * as ReactDOM from "react-dom";
import App from "./App";
import "./styles/index.css";
import "aos/dist/aos.css";

import reportWebVitals from "./reportWebVitals";
import buffer from "buffer";
global.Buffer = buffer.Buffer;

const rootElement = document.getElementById("root");

ReactDOM.render(<App />, rootElement);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
