export const collateralFactor: any = {
  "aurora": 60,
  "nearlend": 60,
  "ref-finance": 60,
  "ethereum": 60,
  "Dai Stablecoin": 90,
  "Tether USD": 90,
  "Wrapped NEAR fungible token": 60
};
