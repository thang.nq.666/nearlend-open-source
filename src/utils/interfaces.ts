import { Dispatch, ReactChild, ReactNode, SetStateAction } from "react";
import { TBalance, TPoolTokenList } from "utils/types";

export interface ChildrenProps {
  children: ReactNode | ReactChild;
}
// AppContext
export interface IAuthContext {
  isLoggedIn?: boolean;
  account?: {
    accountId: string;
    accountName: string;
  };
  setAuthParams: Dispatch<SetStateAction<IAuthContext>>;
}
export interface IUserbalance {
  profile: {
    userBalance: TBalance;
    userAssetFarms: any;
  };
  setUserProfile: Dispatch<SetStateAction<IUserbalance>>;
}
export interface IContractMain {
  contract: any;
  setContract: Dispatch<SetStateAction<IContractMain>>;
}
export interface IWallet {
  wallet: any;
  setWallet: Dispatch<SetStateAction<any>>;
}
export interface IPoolTokenList {
  poolTokenList: TPoolTokenList[];
  setPoolTokenList: Dispatch<SetStateAction<TPoolTokenList[]>>;
}

export interface IPoolNftList {
  poolNftList: TPoolTokenList[];
  setPoolNftList: Dispatch<SetStateAction<TPoolTokenList[]>>;
}
export interface IAppContext
  extends IAuthContext,
    IUserbalance,
    IContractMain,
    IWallet,
    IPoolTokenList,
    IPoolNftList {}
