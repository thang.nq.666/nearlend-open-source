import { formatCurrentDate } from "utils/time-zone";
import nft001 from "images/nft-testnet/001.png";
import nft002 from "images/nft-testnet/002.png";
import nft003 from "images/nft-testnet/003.png";
import nft004 from "images/nft-testnet/004.png";
import nft005 from "images/nft-testnet/005.png";
import nft006 from "images/nft-testnet/006.png";
import nearlogo from "images/logo.png";

export const mockVaultsData = [
  {
    id: 5469,
    pair_value: {
      first: "47.32 NEAR",
      second: "$84,635.13",
    },
    pair_credit: {
      first: "15.14",
      second: "$27",
    },
    out_debt: formatCurrentDate,
    ltv: "0.00",
    health_stt: "NaN",
    nft_url: nft001,
  },
  {
    id: 8287,
    pair_value: {
      first: "2.32 NEAR",
      second: "$84,635.13",
    },
    pair_credit: {
      first: "1.14",
      second: "$25",
    },
    out_debt: formatCurrentDate,
    ltv: "17.81%",
    health_stt: "NaN",
    nft_url: nft002,
  },
  {
    id: 9197,
    pair_value: {
      first: "337.32 NEAR",
      second: "$84,635.13",
    },
    pair_credit: {
      first: "95.14",
      second: "$22",
    },
    out_debt: formatCurrentDate,
    ltv: "17.81%",
    health_stt: "NaN",
    nft_url: nft003,
  },
  {
    id: 9384,
    pair_value: {
      first: "8782.32 NEAR",
      second: "$84,635.13",
    },
    pair_credit: {
      first: "300.14",
      second: "$30",
    },
    out_debt: formatCurrentDate,
    ltv: "34.40%",
    health_stt: "NaN",
    nft_url: nft004,
  },
  {
    id: 8091,
    pair_value: {
      first: "323431.32 NEAR",
      second: "$84,635.13",
    },
    pair_credit: {
      first: "333.14",
      second: "$29",
    },
    out_debt: formatCurrentDate,
    ltv: "0.00",
    health_stt: "NaN",
    nft_url: nft005,
  },
  {
    id: 8514,
    pair_value: {
      first: "07.32 NEAR",
      second: "$84,635.13",
    },
    pair_credit: {
      first: "1.14",
      second: "$21",
    },
    out_debt: formatCurrentDate,
    ltv: "34.40%",
    health_stt: "NaN",
    nft_url: nft006,
  },
];

export const tabsVaults = [
  {
    label: "NFTs in Vaults",
    value: "81",
  },
  {
    label: "Rewards",
    value: nearlogo,
    img: true,
  },
  {
    label: "AVG Floor (7D)",
    value: "50.29 NEAR",
    child: "~$90.67",
  },
  {
    label: "Collateral Value",
    value: "Max 30%",
  },
  {
    label: "Value Locked in Vault",
    value: "$1.44M",
    child: "28.76%",
  },
];
