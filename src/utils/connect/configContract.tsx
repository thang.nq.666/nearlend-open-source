import * as nearAPI from "near-api-js";
import { _walletConnection } from "./contract";
import {
  ENV_HELPER_URL,
  ENV_ID_MAIN_CONTRACT,
  ENV_ID_NETWORK,
  ENV_NODE_URL,
  ENV_WALLET_URL,
} from "utils/constant";
const { connect, keyStores } = nearAPI;
const keyStore = new keyStores.BrowserLocalStorageKeyStore();
export const GAS = 200000000000000;

export const nearConfig = {
  networkId: ENV_ID_NETWORK,
  nodeUrl: ENV_NODE_URL,
  contractName: ENV_ID_MAIN_CONTRACT,
  walletUrl: ENV_WALLET_URL,
  helperUrl: ENV_HELPER_URL,
  headers: {
    "Content-Type": "application/json",
  },
};

export const _near = async function () {
  return await connect({
    deps: {
      keyStore,
    },
    ...nearConfig,
  });
};

export const _contract = function (wallet: any) {
  return new nearAPI.Contract(wallet.account(), ENV_ID_MAIN_CONTRACT, {
    viewMethods: [],
    changeMethods: ["update_asset"],
  });
};

export const handleUpdateConfig = async (
  contractMain: any,
  walletState: any,
  params: any,
  tokenId: string
) => {
  try {
    const args = {
      token_id: tokenId,
      asset_config: params,
    };
    const initNear = await _near();
    const initWallet = _walletConnection(initNear);
    const initContract: any = _contract(initWallet);
    const rs = await initContract.update_asset(args);
  } catch (err) {
    alert(err);
    console.error(err);
  }
};
