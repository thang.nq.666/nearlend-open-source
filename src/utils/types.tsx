export interface IToken {
  tokenId: string;
  supplied: IBorrowed;
  borrowed: IBorrowed;
  reserved: string;
  last_update_timestamp: string;
  config: IConfig;
  supply_apr: string;
  borrow_apr: string;
}

export interface IBorrowed {
  shares: string;
  balance: string;
}

export interface IConfig {
  reserve_ratio: number;
  target_utilization: number;
  target_utilization_rate: string;
  max_utilization_rate: string;
  volatility_ratio: number;
  extra_decimals: number;
  can_deposit: boolean;
  can_withdraw: boolean;
  can_use_as_collateral: boolean;
  can_borrow: boolean;
}

export type TTokenFormat = {
  [token_id: string]: {
    decimals: number;
    extra_decimals: number;
    contract_decimals: number;
    icon: string;
    name: string;
    nameUsd: string;
    usd: number;
    symbol: string;
  };
};

export type TUSDPriceToken = {
  [tokenName: string]: { usd: number };
};

type TTokenSupplied = {
  token_id: string;
  balance: string;
  shares: string;
  apr: string;
};

export type TPoolTokenList = {
  borrow_apr: string;
  borrowed: { shares: string; balance: string };
  config: {
    can_borrow: boolean;
    can_deposit: boolean;
    can_use_as_collateral: boolean;
    can_withdraw: boolean;
    extra_decimals: number;
    max_utilization_rate: string;
    reserve_ratio: number;
    target_utilization: number;
    target_utilization_rate: string;
    volatility_ratio: number;
  };
  farms: [];
  last_update_timestamp: string;
  nft_supplied: [];
  reserved: string;
  supplied: { shares: string; balance: string };
  supply_apr: string;
  tokenId: string;
  token_id: string;
};

export type TBalance = {
  account_id: string;
  supplied: any[];
  nft_supplied: any[];
  borrowed: any[];
  farms?: any[];
  has_non_farmed_assets?: boolean;
  booster_staking?: any;
};

export type TUserAssetFarm = {
  farm_id:
    | string
    | {
        [name: string]: string;
      };
  rewards: AssetFarm[];
};

export type AssetFarm = {
  asset_farm_reward: {
    reward_per_day: string;
    booster_log_base: string;
    remaining_rewards: string;
    boosted_shares: string;
  };
  boosted_shares: string;
  reward_token_id: string;
  unclaimed_amount: string;
};

// export type TNewFormatToken = {
//   [anyName]: {
//     decimals: number;
//     icon: string;
//     name: string;
//     nameUsd: string;
//     price: number;
//     symbol: string;
//   };
// };
// export type TTokenPrice = {
//   [anyName]: {
//     usd: number;
//   };
// };

export enum EQueryKey {
  GET_FORMAT_TOKEN = "GET_FORMAT_TOKEN",
  GET_TOKEN_PRICE_WITH_NAME = "GET_TOKEN_PRICE_WITH_NAME",
}
