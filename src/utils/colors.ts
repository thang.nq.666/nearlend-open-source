export const COLORs = {
  GREEN_LIGHT: "#55d434",
  GREEN_LINE: "#6AA943",
  GREEN_ROW: "#49E454",
  YELLOW: "#FFD166",
};
