import { Container } from "components/common/Container";
import Image from "components/common/Images";
import {
  BackGroundText,
  TextPosition,
  Title,
  WrapperBtn,
} from "components/homepage/styles";
import BIT from "images/3DCoin/BIT.svg";
import ETH from "images/3DCoin/ETH.svg";
import NFT from "images/3DCoin/NFT.svg";
import bgCircleGrid from "images/roadmap/circleGrid.svg";
import bgPhoneOnHand from "images/roadmap/phoneWhand.svg";
import bgRoadmap from "images/roadmap/roadmap.svg";
import styled from "styled-components";
import { COLORs } from "utils/colors";

function RoadMap() {
  return (
    <CustomWrapper>
      <BitCoinBasePosition>
        <BCoinBase />
      </BitCoinBasePosition>
      <EthCoinBasePosition>
        <ECoinBase />
      </EthCoinBasePosition>
      <NftBasePosition>
        <NftBase />
      </NftBasePosition>
      <OnhandPosition>
        <Onhand />
      </OnhandPosition>
      <HighLightBG />
      <RelativeWrapperCustom>
        <Container>
          <BackGroundTitle>
            <Title data-aos="fade-right">
              Product <span className="high-light">Roadmap</span>
            </Title>
            <span>
              to become a top-rated liquidity hub in the NEAR Ecosystem
            </span>
          </BackGroundTitle>
        </Container>
        <RelativeWrapperCustom>
          <Container>
            <TextPosition style={{ top: "10%", width: "100%" }}>
              <div
                data-aos="fade-left"
                style={{ textAlign: "center", opacity: ".5" }}
              >
                <BackGroundText>nearlend</BackGroundText>
              </div>
            </TextPosition>
            <CustomTimeLine style={{ padding: "50px 85px" }}>
              <Image
                src={bgRoadmap}
                style={{
                  width: "100%",
                  height: "100%",
                  backgroundSize: "contain",
                }}
              />
            </CustomTimeLine>
          </Container>
        </RelativeWrapperCustom>
      </RelativeWrapperCustom>
    </CustomWrapper>
  );
}

export default RoadMap;

export const RelativeWrapperCustom = styled.div`
  position: relative;
`;

const BitCoinBasePosition = styled.div`
  position: absolute;
  top: 0;
  z-index: 2;
`;
const EthCoinBasePosition = styled.div`
  position: absolute;
  top: 45%;
  z-index: 2;
  left: 3%;
`;
const NftBasePosition = styled.div`
  position: absolute;
  bottom: 0%;
  z-index: 2;
  right: 3%;
`;
const OnhandPosition = styled.div`
  position: absolute;
  top: 7%;
  right: 0;
  z-index: 2;
`;

const HighLightBG = styled.div`
  position: absolute;
  bottom: 25%;
  z-index: 2;
  right: 3%;
  background: radial-gradient(
    50% 50% at 50% 50%,
    #54a943 0%,
    rgba(106, 169, 67, 0) 100%
  );
  opacity: 0.15;
  width: 500px;
  height: 500px;
  right: -4%;
`;

const Onhand = styled.div`
  background-image: url(${bgCircleGrid});
  background-repeat: no-repeat;
  background-size: contain;
  width: 200px;
  height: 200px;
  position: relative;
  z-index: 2;
  &::after {
    content: "";
    position: absolute;
    background-image: url(${bgPhoneOnHand});
    background-repeat: no-repeat;
    background-size: contain;
    width: 175px;
    height: 175px;
    bottom: -45%;
    right: -10%;
  }
`;

const ECoinBase = styled.div`
  background-image: url(${ETH});
  background-repeat: no-repeat;
  background-size: contain;
  width: 120px;
  height: 120px;
`;
const NftBase = styled.div`
  background-image: url(${NFT});
  background-repeat: no-repeat;
  background-size: contain;
  width: 175px;
  height: 175px;
`;

const BCoinBase = styled.div`
  background-image: url(${BIT});
  background-repeat: no-repeat;
  background-size: contain;
  width: 150px;
  height: 150px;
  position: relative;
  &::after {
    content: "";
    position: absolute;
    inset: 0;
    background: radial-gradient(
      50% 50% at 50% 50%,
      #54a943 0%,
      rgba(106, 169, 67, 0) 100%
    );
    width: 600px;
    height: 600px;
    opacity: 0.4;
    z-index: 1;
    margin: auto;
    left: -133%;
  }
`;

const CustomTimeLine = styled(WrapperBtn)`
  position: relative;
  padding: 50px 85px;
`;

const CustomWrapper = styled(RelativeWrapperCustom)`
  padding: 65px 0;

  @media screen and (max-width: 1200px) {
    .rowFirst {
      top: 8.5% !important;
    }
    .rowTitle {
      display: none;
    }
  }
`;

const BackGroundTitle = styled.div`
  position: relative;
  text-align: center;
  .high-light {
    color: ${COLORs.GREEN_ROW};
    font-weight: 550;
  }
`;
