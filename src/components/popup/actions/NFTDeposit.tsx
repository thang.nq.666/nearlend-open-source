import { useCallback, useContext, useEffect, useState } from "react";
import globalState from "state/globalStore";
import { totalBalance, totalUsdCanBorrow } from "utils/common";
import { useState as hookState, Downgraded } from "@hookstate/core";
import useCurrentToken from "hooks/useCurrentToken";
import { handleBorrow } from "utils/connect/contract";
import { getUsdtOfToken } from "utils/connect/oracle";
import { getBorrowAndSupplyAPY } from "utils/common";
import ActionPopupViewNFT from "components/popup/component/ViewActionPopup";
import { AppContext } from "Contexts/AppContext";

type Props = {
  togglePopup: () => void;
  currentToken: any;
};
const NFTDeposit = ({ togglePopup, currentToken }: Props) => {
  const {
    tokenName,
    tokenNameUsd,
    tokenId,
    tokenIcon,
    tokenSymbol,
    tokenUsdPrice,
  } = useCurrentToken(currentToken);

  const { borrowAPY } = getBorrowAndSupplyAPY(currentToken);

  const { contract, userBalance, poolListToken }: any = useContext(AppContext);

  const { usdTokens }: any = hookState<any>(globalState);
  const usdTokensState = usdTokens.attach(Downgraded).get();

  const [amountToken, setAmountToken] = useState(0);
  const [collateral, setCollatertal] = useState(0);
  const [available, setAvailable] = useState<any>(0);
  const [tokenUsd, setTokenUsd] = useState(0);
  const [totalUsd, setTotalUsd] = useState<any>(0);
  const [error, setError] = useState("");

  const _handleBorrow = useCallback(() => {
    if (available === 0) {
      return setError(`You need to deposit before borrow`);
    } else if (amountToken === 0 || amountToken === null) {
      return setError(`You have to Enter amount of Tokens`);
    } else if (amountToken > available) {
      return setError(`You out of Limits Available`);
    } else if (amountToken < 0) {
      return setError(`You can not borrow with Negative number`);
    } else if (amountToken < 0) {
      return setError(`You can not use Negative number`);
    }
    return handleBorrow(currentToken, amountToken, contract);
  }, [amountToken, available, contract, currentToken]);

  const _onChange = useCallback((e: any) => {
    setAmountToken(e);
  }, []);

  const _onChangeSlider = useCallback((e: any) => {
    setAmountToken(e);
  }, []);

  useEffect(() => {
    const collateral__To__USDT = totalUsdCanBorrow(
      userBalance?.collateral,
      usdTokensState,
      poolListToken
    );

    const borrow__To__USDT = totalBalance(
      userBalance?.borrowed,
      usdTokensState
    );

    const { usd } = usdTokensState[tokenNameUsd] ?? { usd: 0 };
    setTokenUsd(usd);
    setTotalUsd(Number(collateral__To__USDT) - Number(borrow__To__USDT));
  }, [
    collateral,
    userBalance,
    usdTokens,
    usdTokensState,
    poolListToken,
    tokenNameUsd,
  ]);

  useEffect(() => {
    if (totalUsd && tokenUsd) {
      const available = totalUsd / tokenUsd;
      setAvailable(available.toFixed(3));
    }
  }, [tokenUsd, totalUsd, available]);

  useEffect(() => {
    async function initGetUSDPrice() {
      const res = await getUsdtOfToken();
      if (res !== null) {
        usdTokens.set(res);
      }
    }
    const init = async () => await initGetUSDPrice();
    const interval = setInterval(init, 100000000000);

    return () => {
      clearInterval(interval);
    };
  }, [usdTokens]);

  useEffect(() => {
    const currentTokenCollateral =
      userBalance?.collateral?.find((item: any) => item.token_id === tokenId)
        ?.balance ?? 0;
    setCollatertal(currentTokenCollateral);
  }, [tokenId, userBalance?.collateral]);

  return (
    <ActionPopupViewNFT
      textTitle="Borrow"
      togglePopup={togglePopup}
      onChange={_onChange}
      onChangeSlider={_onChangeSlider}
      confirmPopUp={_handleBorrow}
      error={error}
      valAPY={borrowAPY}
      currentToken={currentToken}
    />
  );
};

export default NFTDeposit;
