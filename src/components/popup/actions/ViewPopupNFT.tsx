import { useEffect, useCallback } from "react";
import iconClose from "images/icon-close.png";

type TProps = {
  togglePopup: () => void;
  confirmPopUp?: () => void;
  currentToken: any;
};

const ActionPopupViewNFT = ({
  togglePopup,
  confirmPopUp,
  currentToken,
}: TProps) => {
  const _confirmPopUp = useCallback(() => {
    confirmPopUp && confirmPopUp();
  }, [confirmPopUp]);

  useEffect(() => {
    if (typeof window !== "undefined") {
      const htmlEle = window.document.getElementsByTagName("html")[0];
      const popupEle = window.document.getElementsByTagName("wrap-popup")[0];
      if (popupEle) {
        popupEle.addEventListener("click", () => {
          togglePopup && togglePopup();
        });
      }
      htmlEle.classList.add("popup-open");
    }
    return () => {
      const htmlEle = window.document.getElementsByTagName("html")[0];
      htmlEle.classList.remove("popup-open");
    };
  }, [togglePopup]);

  return (
    <div className="wrap-popup nft-deposit-popup">
      <div className="popup">
        <p className="icon-close" onClick={togglePopup}>
          <img alt="icon-close" src={iconClose} width={12} height={12} />
        </p>

        <div className="wrap-img">
          {currentToken?.type === "video" ? (
            <div className="img">
              <video
                className="video-background"
                autoPlay={true}
                loop
                muted
                playsInline
                controls
                src={currentToken?.imageAsset}
              ></video>
            </div>
          ) : (
            <div
              className="img"
              style={{
                background: `url('${currentToken.imageAsset}') no-repeat center center / cover`,
              }}
            ></div>
          )}
        </div>

        <h3 className="name">{currentToken.name}</h3>
        <div className="nft-row">
          <h4>Price floor:</h4>
          <h5>{currentToken.price} BTC</h5>
        </div>
        <div className="nft-row">
          <h4>Price borrowing:</h4>
          <h5>{currentToken.priceBorrowed} BTC</h5>
        </div>

        <button className="position-relative btn" onClick={_confirmPopUp}>
          Withdraw NFT
        </button>
      </div>
    </div>
  );
};

export default ActionPopupViewNFT;
