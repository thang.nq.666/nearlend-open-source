import { useCallback, useContext, useEffect, useState } from "react";
import { useQueryClient } from "react-query";
import { useState as hookState, Downgraded } from "@hookstate/core";
import globalState from "state/globalStore";
import { handleIncreaseCollateral } from "utils/connect/contract";
import useCurrentToken from "hooks/useCurrentToken";
import { TTokenFormat } from "types/token";
import { QUERY_KEY } from "utils/constant";
import ActionPopupView from "components/popup/component/ViewActionPopup";
import { AppContext } from "Contexts/AppContext";

type Props = {
  togglePopup: () => void;
  currentToken: any;
};
const IncreaseCollateral = ({ togglePopup, currentToken }: Props) => {
  const queryClient = useQueryClient();
  const {
    tokenId,
    tokenName,
    tokenIcon,
    tokenDecimals,
    tokenSymbol,
    tokenDepositBalance,
    borrow_apr,
  } = useCurrentToken(currentToken);

  const { contract, userBalance }: any = useContext(AppContext);

  const [amountToken, setAmountToken] = useState(0);
  const [available, setAvailable] = useState(0);
  const [tokenUsdPrice, setTokenUsdPrice] = useState(0);
  const [error, setError] = useState("");

  const _handleIncreaseCollateral = () => {
    if (tokenDepositBalance === 0) {
      return setError(`You need to Deposit for this Token`);
    } else if (amountToken === 0 || amountToken === null) {
      return setError(`You have to Enter amount of Tokens`);
    } else if (amountToken > available) {
      return setError(`Allow equal or lower value that you deposited`);
    } else if (amountToken < 0) {
      return setError(`You can not use Negative number`);
    }
    return handleIncreaseCollateral(currentToken, amountToken, contract);
  };

  const _onChange = useCallback((e: any) => {
    setAmountToken(e);
  }, []);

  const _onChangeSlider = useCallback((e: any) => {
    setAmountToken(e);
  }, []);

  const _calculate = useCallback(() => {
    const getNewFormatToken = queryClient.getQueryData(
      QUERY_KEY.GET_FORMAT_TOKEN
    ) as unknown as TTokenFormat;

    if (!getNewFormatToken) return;

    const usd: number | undefined = getNewFormatToken?.[tokenId]?.usd;

    setTokenUsdPrice(usd);
  }, [queryClient, tokenId]);

  const _initCalculate = useCallback(() => {
    const interval = setInterval(_calculate, 300);
    return () => {
      clearInterval(interval);
    };
  }, [_calculate]);

  const _initGetAvailable = useCallback(() => {
    const currentTokenSupplied: number =
      userBalance?.supplied?.find((item: any) => item.token_id === tokenId)
        ?.balance ?? 0;

    setAvailable(currentTokenSupplied / 10 ** tokenDecimals);
  }, [tokenDecimals, tokenId, userBalance?.supplied]);

  useEffect(() => {
    _initCalculate();
  }, [_initCalculate]);

  useEffect(() => {
    _initGetAvailable();
  }, [_initGetAvailable]);

  return (
    <ActionPopupView
      textTitle="Increase Collateral"
      togglePopup={togglePopup}
      onChange={_onChange}
      onChangeSlider={_onChangeSlider}
      confirmPopUp={_handleIncreaseCollateral}
      error={error}
      valAPY={borrow_apr}
      currentToken={{
        available,
        tokenName,
        tokenSymbol,
        tokenUsdPrice,
        tokenIcon,
      }}
    />
  );
};

export default IncreaseCollateral;
