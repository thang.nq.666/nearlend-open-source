import { useEffect, useState, useCallback, useContext } from "react";
import { useQueryClient } from "react-query";
import { handleDecreaseCollateral } from "utils/connect/contract";
import useCurrentToken from "hooks/useCurrentToken";
import { getTotalBalanceTokenPriceUSD } from "utils/common";
import { TTokenFormat } from "types/token";
import { QUERY_KEY } from "utils/constant";
import ActionPopupView from "components/popup/component/ViewActionPopup";
import { AppContext } from "Contexts/AppContext";

type Props = {
  togglePopup: () => void;
  currentToken: any;
};
const DecreaseCollateral = ({ togglePopup, currentToken }: Props) => {
  const queryClient = useQueryClient();
  const {
    tokenName,
    tokenId,
    tokenIcon,
    tokenDecimals,
    tokenSymbol,
    tokenConfig,
    borrow_apr,
  } = useCurrentToken(currentToken);

  const { contract, profile, poolTokenList } = useContext(AppContext);

  const [amountToken, setAmountToken] = useState(0);
  const [tokenUsdPrice, setTokenUsdPrice] = useState(0);
  const [available, setAvailable] = useState(0);
  const [error, setError] = useState("");

  // Other
  const _handleDecreaseCollateral = useCallback(() => {
    if (available === 0) {
      return setError(`You need to deposit before borrow`);
    } else if (amountToken === 0 || amountToken === null) {
      return setError(`You have to Enter amount of Tokens`);
    } else if (amountToken > available) {
      return setError(`You out of Limits Available`);
    } else if (amountToken < 0) {
      return setError(`You can not borrow with Negative number`);
    }
    return handleDecreaseCollateral(
      currentToken,
      Number(amountToken.toFixed(7)),
      contract
    );
  }, [amountToken, available, contract, currentToken]);

  const _onChange = useCallback((e: any) => {
    setAmountToken(e);
  }, []);

  const _onChangeSlider = useCallback((e: any) => {
    setAmountToken(e);
  }, []);

  const _setAvailable = useCallback(() => {
    const getNewFormatToken = queryClient.getQueryData(
      QUERY_KEY.GET_FORMAT_TOKEN
    ) as unknown as TTokenFormat;

    const currentTokenCollateral =
      profile?.userBalance?.supplied?.find(
        (item: any) => item.token_id === tokenId
      )?.balance ?? 0;

    const currentTokenUSD =
      (Math.abs(currentTokenCollateral) / 10 ** tokenDecimals) *
      tokenConfig.usd;

    const borrowedTokensUSD = getTotalBalanceTokenPriceUSD(
      "borrowed",
      profile?.userBalance?.borrowed,
      getNewFormatToken,
      poolTokenList
    );

    const calculateAvailable =
      (+currentTokenUSD - +borrowedTokensUSD) / tokenConfig.usd;
    const checkCalculate = calculateAvailable < 0.0001 ? 0 : calculateAvailable;

    setAvailable(checkCalculate);
    setTokenUsdPrice(tokenConfig.usd);
  }, [
    poolTokenList,
    queryClient,
    tokenConfig.usd,
    tokenDecimals,
    tokenId,
    profile,
  ]);

  useEffect(() => {
    const init = () => _setAvailable();
    const interval = setInterval(init, 100);
    return () => {
      clearInterval(interval);
    };
  }, [_setAvailable]);

  useEffect(() => {
    _setAvailable();
  }, [_setAvailable]);

  return (
    <ActionPopupView
      textTitle="Decrease Collateral"
      togglePopup={togglePopup}
      onChange={_onChange}
      onChangeSlider={_onChangeSlider}
      confirmPopUp={_handleDecreaseCollateral}
      error={error}
      valAPY={borrow_apr}
      currentToken={{
        available,
        tokenName,
        tokenSymbol,
        tokenUsdPrice,
        tokenIcon,
      }}
    />
  );
};

export default DecreaseCollateral;
