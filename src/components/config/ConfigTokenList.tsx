import { IToken } from "types/token";
import ConfigTokenItem from "components/config/ConfigTokenItem";

function ConfigTokenList({ tokenList, _openPopupUpdateConfigPtc }: any) {
  return (
    <>
      {tokenList &&
        tokenList.length > 0 &&
        tokenList.map((item: IToken, idx: number) => {
          if (!item) return null;
          return (
            <ConfigTokenItem
              key={item.tokenId}
              item={item}
              openPopupUpdateConfigPtc={_openPopupUpdateConfigPtc}
            />
          );
        })}
    </>
  );
}

export default ConfigTokenList;
