import { shortBalance } from "utils/common";
import { QUERY_KEY, tokenFormat } from "utils/constant";
import { TTokenFormat } from "types/token";
import { useQueryClient } from "react-query";
import { useCallback, useEffect, useState } from "react";

function ConfigTokenItem({ item, openPopupUpdateConfigPtc }: any) {
  const queryClient = useQueryClient();
  const { tokenId } = item;

  const icon = tokenFormat[tokenId.toString()]?.icon;
  const tokenSymbol = tokenFormat[tokenId.toString()]?.symbol;
  const [tokenUsd, setTokenUsd] = useState(0);

  const _calculate = useCallback(() => {
    const getNewFormatToken = queryClient.getQueryData(
      QUERY_KEY.GET_FORMAT_TOKEN
    ) as unknown as TTokenFormat;

    if (!getNewFormatToken) return;
    const usd = getNewFormatToken[tokenId].usd;

    setTokenUsd(usd);
  }, [queryClient, tokenId]);

  const _initCalculate = useCallback(() => {
    const initCalculate = () => _calculate();
    const interval = setInterval(initCalculate, 300);
    return () => {
      clearInterval(interval);
    };
  }, [_calculate]);

  useEffect(() => {
    _initCalculate();
  }, [_initCalculate]);

  return (
    <div className="wrap-pool">
      <div className="mini asset market-flex">
        <img className="icon" src={icon} width={32} height={32} alt="Logo" />
        <div className="coin-des">
          <p className="top coin color-white fwb">{tokenSymbol}</p>
          <p className="color-space-gray">${shortBalance(tokenUsd)}</p>
        </div>
      </div>
      <div
        onClick={(e) => openPopupUpdateConfigPtc(e, item)}
        className="action color-white on-desktop update-v2"
      >
        <button className="btn-base button-basic-deposit">Update</button>
      </div>
    </div>
  );
}

export default ConfigTokenItem;
