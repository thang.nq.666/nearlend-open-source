import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { handleUpdateConfig } from "utils/connect/configContract";
import useCurrentToken from "hooks/useCurrentToken";
import { shortBalance } from "utils/common";
import { AppContext } from "Contexts/AppContext";
import iconClose from "../../images/icon-close.png";
import { InputNumber, Input } from "antd";
import { useQueryClient } from "react-query";
import { handleGetConfigToken } from "utils/connect/contract";

type Props = {
  togglePopup: () => void;
  currentToken: any;
};
const ConfigProtocolContract = ({ togglePopup, currentToken }: Props) => {
  const initInterval = useRef<any>();
  const {
    tokenId,
    tokenName,
    tokenDecimals,
    tokenSymbol,
    tokenConfig,
    tokenIcon,
    tokenRatio,
  } = useCurrentToken(currentToken);
  const queryClient = useQueryClient();
  const { userBalance, wallet, contract, poolTokenList } =
    useContext(AppContext);

  const textTitle = "Update Config";

  const [reserveRatio, setReserveRatio] = useState(0);
  const [targetUtilization, setTargetUtilization] = useState(0);
  const [targetUtilizationRate, setTargetUtilizationRate] = useState("");
  const [maxUtilizationRate, setMaxUtilizationRate] = useState("");
  const [volatilityRatio, setVolatilityRatio] = useState(0);
  const [extraDecimals, setExtraDecimals] = useState(0);
  const [canWithdraw, setCanWithdraw] = useState(1);
  const [canDeposit, setCanDeposit] = useState(1);
  const [canUseAsCollateral, setCanUseAsCollateral] = useState(1);
  const [canBorrow, setCanBorrow] = useState(1);
  const [currentConfig, setCurrentConfig] = useState({});
  const [error, setError] = useState("");
  const [tokenUsdPrice, setTokenUsdPrice] = useState<number>();

  let params = {
    reserve_ratio: 1000,
    target_utilization: 8000,
    target_utilization_rate: "1000000000001243680655546223",
    max_utilization_rate: "1000000000017745300226420217",
    volatility_ratio: 9500,
    extra_decimals: 0,
    can_deposit: true,
    can_withdraw: true,
    can_use_as_collateral: true,
    can_borrow: false,
  };

  const _get = useCallback(async () => {
    const currentConfig = await handleGetConfigToken(contract, wallet, [
      tokenId,
    ]);
    // console.log('currentConfig: ', currentConfig);
    setReserveRatio(currentConfig.reserve_ratio);
    setTargetUtilization(currentConfig.target_utilization);
    setTargetUtilizationRate(currentConfig.target_utilization_rate);
    setMaxUtilizationRate(currentConfig.max_utilization_rate);
    setVolatilityRatio(currentConfig.volatility_ratio);
    setExtraDecimals(currentConfig.extra_decimals);
    setCanWithdraw(currentConfig.can_withdraw ? 1 : 0);
    setCanDeposit(currentConfig.can_deposit ? 1 : 0);
    setCanUseAsCollateral(currentConfig.can_use_as_collateral ? 1 : 0);
    setCanBorrow(currentConfig.can_borrow ? 1 : 0);
    setCurrentConfig(currentConfig);
  }, [queryClient, tokenId]);

  useEffect(() => {
    _get();
  }, [_get]);

  const _handleUpdate = useCallback(() => {
    params = {
      reserve_ratio: reserveRatio,
      target_utilization: targetUtilization,
      target_utilization_rate: targetUtilizationRate,
      max_utilization_rate: maxUtilizationRate,
      volatility_ratio: volatilityRatio,
      extra_decimals: extraDecimals,
      can_deposit: canDeposit == 1,
      can_withdraw: canWithdraw == 1,
      can_use_as_collateral: canDeposit == 1,
      can_borrow: canBorrow == 1,
    };
    return handleUpdateConfig(contract, wallet, params, tokenId);
  }, [contract, wallet, params, tokenId]);

  const _onChange = useCallback((e: any, type: string) => {
    switch (type) {
      case "reserveRatio":
        setReserveRatio(e);
        break;
      case "targetUtilization":
        setTargetUtilization(e);
        break;
      case "targetUtilizationRate":
        setTargetUtilizationRate(e);
        break;
      case "maxUtilizationRate":
        setMaxUtilizationRate(e);
        break;
      case "volatilityRatio":
        setVolatilityRatio(e);
        break;
      case "extraDecimals":
        setExtraDecimals(e);
        break;
      case "canWithdraw":
        setCanWithdraw(e);
        break;
      case "canDeposit":
        setCanDeposit(e);
        break;
      case "canUseAsCollateral":
        setCanUseAsCollateral(e);
        break;
      case "canBorrow":
        setCanBorrow(e);
        break;
    }
  }, []);

  const _calculate = useCallback(() => {
    const { usd } = tokenConfig || { usd: 1 };
    setTokenUsdPrice(usd);
  }, [tokenConfig]);

  const _initCalculate = useCallback(() => {
    initInterval.current = setInterval(_calculate, 300);
  }, [_calculate]);

  useEffect(() => {
    _initCalculate();
    return () => {
      clearInterval(initInterval.current);
    };
  }, [_initCalculate]);

  return (
    <div className={`wrap-popup`}>
      <div className="popup">
        <p className="icon-close" onClick={togglePopup}>
          <img alt="icon-close" src={iconClose} width={12} height={12} />
        </p>
        <h4 className={`title`}>{textTitle}</h4>
        <div className="wrap-icon-name">
          <p className="icon">
            <img
              className="icon"
              src={tokenIcon}
              width={54}
              height={54}
              alt="Logo"
            />
          </p>
          <div className="wrap-price-apy">
            <div className="wrap-price">
              <p className="icon-name">{tokenName}</p>
              <p className="price">${shortBalance(tokenUsdPrice)}</p>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-6">
            <h3 className="available">reserve_ratio</h3>
            <div className="wrap-input-number">
              <InputNumber
                className="input-number"
                defaultValue={0}
                type="number"
                keyboard={true}
                value={parseFloat(reserveRatio?.toFixed(2))}
                onChange={(e) => _onChange(e, "reserveRatio")}
              />
            </div>
          </div>
          <div className="col-6">
            <h3 className="available">target_utilization</h3>
            <div className="wrap-input-number">
              <InputNumber
                className="input-number"
                defaultValue={0}
                type="number"
                keyboard={true}
                value={parseFloat(targetUtilization?.toFixed(2))}
                onChange={(e) => _onChange(e, "targetUtilization")}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <h3 className="available">target_utilization_rate</h3>
            <div className="wrap-input-number">
              <Input
                className="input-text"
                type="text"
                value={targetUtilizationRate}
                onChange={(e) =>
                  _onChange(e.target.value, "targetUtilizationRate")
                }
              />
            </div>
          </div>
          <div className="col-6">
            <h3 className="available">max_utilization_rate</h3>
            <div className="wrap-input-number">
              <Input
                className="input-text"
                type="text"
                value={maxUtilizationRate}
                onChange={(e) =>
                  _onChange(e.target.value, "maxUtilizationRate")
                }
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-6">
            <h3 className="available">volatility_ratio</h3>
            <div className="wrap-input-number">
              <InputNumber
                className="input-number"
                defaultValue={0}
                type="number"
                keyboard={true}
                value={parseFloat(volatilityRatio?.toFixed(2))}
                onChange={(e) => _onChange(e, "volatilityRatio")}
              />
            </div>
          </div>
          <div className="col-6">
            <h3 className="available">extra_decimals</h3>
            <div className="wrap-input-number">
              <InputNumber
                className="input-number"
                defaultValue={0}
                type="number"
                keyboard={true}
                value={parseFloat(extraDecimals?.toFixed(2))}
                onChange={(e) => _onChange(e, "extraDecimals")}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-6 col-checkbox">
            <h3 className="available">can_deposit</h3>
            <div className="wrap-input-checkbox">
              <Input
                className="input-number"
                defaultValue={1}
                type="checkbox"
                checked={canDeposit == 1}
                value={canDeposit}
                onChange={(e) => _onChange(e, "canDeposit")}
              />
            </div>
          </div>
          <div className="col-6 col-checkbox">
            <h3 className="available">can_withdraw</h3>
            <div className="wrap-input-checkbox">
              <Input
                className="input-number"
                defaultValue={1}
                type="checkbox"
                checked={canWithdraw == 1}
                value={canWithdraw}
                onChange={(e) => _onChange(e, "canWithdraw")}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-6 col-checkbox">
            <h3 className="available">can_use_as_collateral</h3>
            <div className="wrap-input-checkbox">
              <Input
                className="input-number"
                defaultValue={1}
                type="checkbox"
                checked={canUseAsCollateral == 1}
                value={canUseAsCollateral}
                onChange={(e) => _onChange(e, "canUseAsCollateral")}
              />
            </div>
          </div>
          <div className="col-6 col-checkbox">
            <h3 className="available">can_borrow</h3>
            <div className="wrap-input-checkbox">
              <Input
                className="input-number"
                defaultValue={1}
                type="checkbox"
                checked={canBorrow == 1}
                value={canBorrow}
                onChange={(e) => _onChange(e, "canBorrow")}
              />
            </div>
          </div>
        </div>
        <div className="bg-white position-relative wrap-white">
          {error && <p className="text-error">* {error} </p>}
          <button className={`position-relative btn`} onClick={_handleUpdate}>
            {textTitle}
          </button>
        </div>
      </div>
    </div>
  );
};

export default ConfigProtocolContract;
