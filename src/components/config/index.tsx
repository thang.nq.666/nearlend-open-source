import {useContext, useEffect, useState} from "react";
import "./config.scss";
import ConfigTokenList from "components/config/ConfigTokenList";
import { Container } from "components/common/Container";
import { AppContext } from "Contexts/AppContext";
import ConfigProtocolContract from "./ConfigProtocolContract";
import {
  RequireLogin
} from "components/popup";
import {useNavigate} from "react-router-dom";
import {DebuggerContext} from "../debug/FlagDebugger";

function MarketApp() {
  const history = useNavigate();
  const { isUrlDebugger } = useContext(DebuggerContext);
  const { poolTokenList, isLoggedIn, wallet, setAuthParams }: any = useContext(AppContext);

  const [isShowPopupUpdateConfigPtc, setIsShowPopupUpdateConfigPtc] = useState(false);
  const [isShowPopupRequireLogin, setIsShowPopupRequireLogin] = useState(false);
  const [tokenChose, setTokenChose]: any = useState(null);
  const [setIsShowPopupPopupRequire] = useState(false);
  const [popupRequire, setPopupRequire] = useState({
    textTitle: "",
    textConfirm: "",
    textCancel: "",
    handleConfirm: () => {},
  });

  const accountIdsPtc = [
    'config_test1.testnet',
    'lam-test6.testnet'
  ];

  const accountIdsOracle = [
    'config_test2.testnet',
    'lam-test6.testnet'
  ];

  const logout = async () => {
    await wallet.signOut();
    setAuthParams((prev: any) => ({
      ...prev,
      isLoggedIn: false,
      account: {
        ...prev.account,
        accountName: "",
      },
    }));
    history({
      pathname: "/",
      search: isUrlDebugger ? "?debug=1" : "",
    });
  };

  const setUpPopup = (e: any, item: any) => {
    e.preventDefault();
    setTokenChose(item);
  };

  const openPopupUpdateConfigPtc = async (e: any, item: any) => {
    setUpPopup(e, item);
    handleValidate('protocolContract');
  };

  const _handleTogglePopupConfigPtc = () => {
    setIsShowPopupUpdateConfigPtc((prevState) => !prevState);
  };

  const _handleTogglePopupRequireLogin = () => {
    setPopupRequire({
      textTitle: "You need to Login!",
      textConfirm: "LogIn",
      textCancel: "Close",
      handleConfirm: () => {},
    });
    setIsShowPopupRequireLogin((prev) => !prev);
  };

  const handleValidate = (type: string) => {
    if (!isLoggedIn) {
      return setIsShowPopupRequireLogin(true);
    } else if (isLoggedIn) {
      switch (type) {
        case 'protocolContract':
          if (!accountIdsPtc.includes(wallet._authData.accountId)) {
            setPopupRequire({
              textTitle: "You are not owner.",
              textConfirm: "Sign Out",
              textCancel: "Close",
              handleConfirm: () => {},
            });
            setIsShowPopupRequireLogin(true);
          } else {
            setIsShowPopupUpdateConfigPtc(true);
          }
          break;
        case 'oracleContract':
          break;
        default:
          return null;
      }
    }
  };

  return (
    <div className="homepage market-app config">
      <Container>
        <div className="wrap-market">
          <h5 className="title">
            Configuration for Nearlend Protocol Contract
          </h5>
          <div className="wrap-label">
            <p className="w-100">Asset</p>
            <p className="w-150">Update</p>
          </div>
          <div className="pools">
            <ConfigTokenList
              setTurnOff={setIsShowPopupPopupRequire}
              tokenList={poolTokenList}
              _openPopupUpdateConfigPtc={openPopupUpdateConfigPtc}
            />
          </div>
        </div>
      </Container>
      <Container>
        <div className="wrap-market">
          <h5 className="title">Configuration for Price Oracle Contract</h5>
          <div className="pools">
            <div className="wrap-pool">
              <div className="mini asset market-flex">Duration Sec</div>
              <div className="mini asset market-flex">1</div>
              <div className="action color-white on-desktop update-v2">
                <button className="btn-base button-basic-deposit">
                  Update
                </button>
              </div>
            </div>
            <div className="wrap-pool">
              <div className="mini asset market-flex">Account Bot</div>
              <div className="mini asset market-flex">
                price-oracle1.testnet
                <br />
                price-oracle2.testnet
                <br />
                price-oracle3.testnet
                <br />
              </div>
              <div className="action color-white on-desktop update-v2">
                <button className="btn-base button-basic-deposit">Add</button>
                <button className="btn-base button-basic-deposit">
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </Container>
      {isShowPopupUpdateConfigPtc && (
        <ConfigProtocolContract
          currentToken={tokenChose}
          togglePopup={_handleTogglePopupConfigPtc}
        />
      )}
      {isShowPopupRequireLogin && (
        <RequireLogin
          textTitle={
            popupRequire.textTitle ||
            "You need to Login!"
          }
          textConfirm={popupRequire.textConfirm || "Log In"}
          textCancel={popupRequire.textCancel || "Cancel"}
          togglePopup={_handleTogglePopupRequireLogin}
        />
      )}
    </div>
  );
}

export default MarketApp;
