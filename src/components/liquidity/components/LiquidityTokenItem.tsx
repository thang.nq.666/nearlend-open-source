import React from "react";
import useCurrentToken from "hooks/useCurrentToken";
import { formatCash } from "utils/common";
import Big from "big.js";
import "components/liquidity/style.scss";
import nel from "images/token_nel.png";
import { ENV_ID_TOKEN_NEL } from "utils/constant";

export const LiquidityTokenItem = React.memo(({ data }: any) => {
  const { tokenIcon, tokenSymbol, tokenUsdPrice } = useCurrentToken({
    token_id: data.id,
  });

  const total_nel_rewards = data.rewards?.reduce((acc: any, curr: any) => {
    if (curr.reward_token_id === ENV_ID_TOKEN_NEL) {
      const reward_per_day = curr?.asset_farm_reward.reward_per_day || 0;
      const pool_boosted_shares = curr?.asset_farm_reward.boosted_shares || 0;
      const boosted_shares = curr?.boosted_shares || 0;

      const result = Big(reward_per_day)
        .div(Big(pool_boosted_shares))
        .mul(Big(boosted_shares))
        .div(Big(10).pow(24))
        .toNumber();

      acc += result;
    }
    return acc;
  }, 0);

  return (
    <div className="liquidity-token">
      <div className="liquidity-token-width liquidity-token__symbol">
        <img
          className="icon"
          src={tokenIcon || nel}
          width={32}
          height={32}
          alt="Logo"
        />
        <div className="coin-des">
          <p className="top coin color-white fwb">{tokenSymbol || "N/A"}</p>
          <p className="color-space-gray">${formatCash(tokenUsdPrice)}</p>
        </div>
      </div>
      <div className="liquidity-token-width liquidity-token__price">
        <h3 className="amount">0</h3>
        <h3 className="dollar">$0</h3>
      </div>
      {/* <div className="liquidity-token-width liquidity-token__button">
        <button className={`common-button btn`} onClick={() => {}}>
          Claim
        </button>
      </div> */}
    </div>
  );
});
