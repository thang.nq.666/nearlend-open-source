import { Button as AntButton } from "antd";
import styled from "styled-components";

const Button = (props: any) => {
  return (
    <ButtonStyle {...props}>
      <span className="text-inside">{props.children}</span>
    </ButtonStyle>
  );
};

const ButtonStyle = styled(AntButton)`
  background: transparent;
  border-radius: 6px;
  padding: 15px 35px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  text-transform: capitalize;
  width: 150px;
  height: 60px;
  gap: 10px;
  color: white;
  font-size: 14px;
  .text-inside {
    font-weight: 700;
  }
`;

export default Button;
