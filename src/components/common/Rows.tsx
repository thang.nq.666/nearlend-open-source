import styled from "styled-components";
import "../../row.css";
import { COLORs } from "utils/colors";

interface RowProps {
  style?: object;
  width?: number;
  height?: number;
  className?: string;
  opacity?: number;
  isFlat?: boolean;
  isCircle?: boolean;
}
interface Props {
  width?: number;
  height?: number;
  className?: string;
  opacity?: number;
  isflat?: boolean;
  isCircle?: boolean;
}

const Rows = (props: RowProps) => {
  const { width, height, opacity, isFlat, isCircle, className } = props;
  return (
    <div {...props} className={className}>
      <div className="r-path-fade">
        {isCircle ? (
          <div className="r-container-circle">
            <Circle style={{ left: "3.5%" }} />
            <Circle style={{ left: "40%" }} />
            <Circle style={{ left: "74%" }} />
          </div>
        ) : (
          ""
        )}
        <ROW width={width} isflat={isFlat} height={height} opacity={opacity} />
      </div>
    </div>
  );
};

export default Rows;

const Circle = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 100px;
  border: 1px solid ${COLORs.GREEN_LIGHT};
  position: relative;
  top: 5px;
`;

const ROW = styled.div<Props>`
  height: ${(props) => (props.height ? `${props.height}px` : "1px")};
  width: ${(props) => (props.width ? `${props.width}px` : "100%")};
  background-image: ${(props) =>
    props.isflat
      ? `linear-gradient(
    to right,
    ${COLORs.GREEN_ROW},
    ${COLORs.GREEN_ROW}
  )`
      : `linear-gradient( to left,rgba(73,228,84,0.2),rgba(73,228,84,1),rgba(73,228,84,0.2) );`};
  opacity: ${(props) => (props.opacity ? `${props.opacity}` : "1")};
`;
