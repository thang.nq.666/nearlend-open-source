import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { WhitePaperUrl } from "utils/constant";

export default function NftDetail() {
  let navigate: any = useNavigate();
  const _goBack = () => {
    navigate(-1);
  };
  return (
    <main
      className="white-paper"
      style={{
        position: "relative",
        zIndex: 9999,
        minHeight: "100vh",
        height: "100%",
        backgroundColor: "#fff",
      }}
    >
      <object
        style={{
          position: "relative",
          zIndex: 9999,
          width: "100%",
          minHeight: "100vh",
          height: "100%",
          border: "none",
        }}
        data={WhitePaperUrl}
        type="application/pdf"
      >
        <p className="pdf-not-work">
          Your web browser doesn't have a PDF plugin. Instead you can{" "}
          <Link to={WhitePaperUrl} target="_blank" download>
            click here to download the PDF file.
          </Link>
          <br />
          <br />
          <p>
            <a onClick={_goBack}>{`Go back`}</a>
          </p>
        </p>
      </object>
    </main>
  );
}
