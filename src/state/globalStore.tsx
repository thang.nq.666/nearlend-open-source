import { createState } from "@hookstate/core";

type TTokenSupplied = {
  token_id: string;
  balance: string;
  shares: string;
  apr: string;
};

export type TUserBalanceState = {
  account_id: string;
  supplied: TTokenSupplied[];
  nft_supplied: any[];
  borrowed: TTokenSupplied[];
  farms: any[];
  has_non_farmed_assets: boolean;
  booster_staking: any;
};
const globalState: any = createState<any>({
  near: null,
  wallet: null,
  contract: null,
  balance: null,
  userBalance: null,
  poolListToken: null,
  isLogged: false,
  usdTokens: null,
  tokensPrice: null,
});

export default globalState;
