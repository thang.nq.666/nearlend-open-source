module.exports = (api) => {
  const babelEnv = api.env();
  const plugins = [
    [
      require.resolve("babel-plugin-module-resolver"),
      {
        root: ["./src"],
        extensions: [".tsx", ".jsx", ".js", ".json", ".ts"],
      },
    ],
  ];
  if (babelEnv !== "development") {
    // 'transform-remove-console' production env eslint version <6
    // plugins.push(['transform-remove-console', {exclude: ['error', 'warn']}]);
  }
  return {
    plugins,
  };
};
