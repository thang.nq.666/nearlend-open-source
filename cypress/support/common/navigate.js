/* eslint-disable no-undef */

const _connect_with_NEAR = (id_account_wallet) => {
  if (id_account_wallet)
    cy.get("h3")
      .contains("Connect with NEAR")
      .parent()
      .contains(id_account_wallet)
      .click();
  cy.get("h3").contains("Connect with NEAR").parent().contains("Next").click();

  cy.get("h3")
    .contains("Connecting with:")
    .parent()
    .contains("button", "Connect")
    .click()
    .wait(7000);
};

const _connectWallet = (test_id_account_wallet, test_seed_phrase) => {
  cy.get("[class='nav-menu']")
    .contains("Connect Wallet")
    .click()
    .then(() => {
      cy.get("h3").then(($btn) => {
        if ($btn.text() === "Connect with NEAR") {
          // CASE ONE - browser check it HAVE BEEN input seed-phrase
          _connect_with_NEAR(test_id_account_wallet);
        } else {
          // CASE TWO - browser check it NEVER input seed-phrase
          // do something else
          cy.get("h1")
            .contains("NEAR is here.")
            .then(() => {
              cy.get("h1")
                .contains("NEAR is here.")
                .parent()
                .contains("button", "Import Existing Account")
                .click();

              //At Import Account - wallet page
              cy.get("h1")
                .contains("Import Account")
                .parent()
                .contains("button", "Recover Account")
                .click();

              //At Recover using Passphrase - wallet page
              cy.get("h1")
                .contains("Recover using Passphrase")
                .parent()
                .get('[data-test-id="seedPhraseRecoveryInput"]')
                .focus()
                .type(test_seed_phrase)
                .parent()
                .contains("Find My Account")
                .click();

              //Connect with Near - wallet page
              //1. Auto click Next to Login
              _connect_with_NEAR(test_id_account_wallet);
            });
        }
      });
    });
};

export class TestNavigate {
  navigate(destination) {
    cy.visit(destination);
  }

  market() {
    cy.visit("/app?debug=1");
  }

  connectWallet(test_id_account_wallet, test_seed_phrase) {
    _connectWallet(test_id_account_wallet, test_seed_phrase);
  }
}

export const onNavigateTo = new TestNavigate();
