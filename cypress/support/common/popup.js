/* eslint-disable no-undef */

const _openPopup = (popup_name, token_id) => {
  cy.get('[class="wrap-market"]')
    .find(`[data-cy="${token_id}"]`)
    .contains(popup_name)
    .click();
};

export class Popup {
  openPopup(popup_name, token_id) {
    _openPopup(popup_name, token_id);
  }
}

export const onPopup = new Popup();
