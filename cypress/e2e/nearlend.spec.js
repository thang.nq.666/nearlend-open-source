/* eslint-disable no-undef */
/// <reference types="cypress" />

const { onNavigateTo } = require("../support/common/navigate");
const { onPopup } = require("../support/common/popup");

const test_id_account_wallet = "tn666.testnet";
const test_seed_phrase_wallet =
  "drink corn draw oval lamp cup season win sister approve spray entire";

describe("Hello World, First test-case on NEARLEND", () => {
  it.only("Test Cases", () => {
    describe("Test-cases BEFORE connect wallet", () => {
      // ... test-case
    });

    describe("Connect Wallet", () => {
      onNavigateTo.market();
      onNavigateTo.connectWallet(
        test_id_account_wallet,
        test_seed_phrase_wallet
      );
      onPopup.openPopup("Deposit", "dai.fakes.testnet");
    });

    describe("Test-cases AFTER connect wallet", () => {
      // ... test-case
    });
  });
});
